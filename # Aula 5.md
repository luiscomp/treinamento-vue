# Aula 5

<div style="margin-top: 10px">
    <p align="center">
        <img src="https://nuxtjs.org/logos/nuxt.svg" width="120" alt="atomic designer">
    </p>
    <p align="center">
        <b>NuxtJS</b>
    </p>
</div>

O Nuxt JS é um framework JavaScript criado Sebastien Chopin e Alexander Chopin em 2016. Ele é mantido pela comunidade, assim como o Vue JS. Note que ele é um framework desenvolvido para outro framework. Por que isso é necessário?

- Vue por si só já é muito poderosa;
- Vue por padrão é uma SPA (Single Page Application);
- Se preocupe com apenas o essencial;
- VueRouter, Vuex, vue-meta;

---

## 5.1 `Features`
Nuxt dar ao desenvolvedor features importantes para que o foco seja exclusivamente na aplicação

- Automatic Routes
    ```txt
    .
    ├── pages/
    |   ├── user/
    |   |   ├── index.vue ----- http://localhost:3000/user
    |   |   └──one.vue -------- http://localhost:3000/user/one
    └── index.vue ------------- http://localhost:3000/
    ```

- Navegação

    ```html
    <NuxtLink to="/">Início</NuxtLink>
    ```
- Store (Arquitetura Flux)

## 5.2 `Estrutura de Diretórios`
- **.nuxt** ⇢ *diretório de build*
- **assets** ⇢ *assets não compilados, como arquivos `Stylus` ou `Sass`, imagens ou fontes.*
- **components** ⇢ *contém seus componentes `Vue.js`*
- **dist** ⇢ *gerado dinamicamente ao usar o comando `nuxt generate`*
- **layouts** ⇢ *contém layouts pré definidos para as páginas*
- **middleware** ⇢ *funções personalizadas*
- **pages** ⇢ *contém suas views e rotas da aplicação*
- **plugins** ⇢ *contém seus plugins Javascript que você deseja executar antes de instanciar a aplicação raiz `Vue.js`*
- **static** ⇢ *diretamente mapeado para a raiz do servidor e contém arquivos que provavelmente não serão alterados.*
- **store** ⇢ *contém seus arquivos da `Store Vuex`*
- **`nuxt.config.js`** ⇢ *arquivo de configuração do projeto `Nuxt`*
