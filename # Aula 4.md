# Aula 4
Dados Computados e Observadores

## 4.1 `Dados Computados`
`index.html`
```html
<div id="example">
  <p>Mensagem original: "{{ message }}"</p>
  <p>Mensagem ao contrário: "{{ reversedMessage }}"</p>
</div>
```
`script.js`
```javascript
var vm = new Vue({
  el: '#example',
  data: {
    message: 'Olá Vue'
  },
  computed: {
    // uma função "getter" computada (computed getter)
    reversedMessage: function () {
      // `this` aponta para a instância Vue da variável `vm`
      return this.message.split('').reverse().join('')
    }
  }
})
```

## 4.2 `Dados Computados vs. Observadores`
`index.html`
```html
<div id="demo">{{ fullName }}</div>
```

### 4.2.1 Watch
`script.js`
```javascript
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar',
    fullName: 'Foo Bar'
  },
  watch: {
    firstName: function (val) {
      this.fullName = val + ' ' + this.lastName
    },
    lastName: function (val) {
      this.fullName = this.firstName + ' ' + val
    }
  }
})
```

### 4.2.2 Computed
`script.js`
```javascript
var vm = new Vue({
  el: '#demo',
  data: {
    firstName: 'Foo',
    lastName: 'Bar'
  },
  computed: {
    fullName: function () {
      return this.firstName + ' ' + this.lastName
    }
  }
})
```