# Aula 2
Framework VueJS

### `Pré requisitos`
- [jsfiddle](https://jsfiddle.net/)

---
## 2.1 `Component Principal e Renderização Declarativa`
`index.html`
```html
<div id="app">
  {{ message }}
</div>
```
`script.js`
```javascript
var app = new Vue({
  el: '#app',
  data: {
    message: 'Olá Vue!'
  }
})
```

## 2.2 `Diretivas`
São atributos especiais providos pelo Vue.

`index.html`
```html
<div id="app">
  <span v-bind:title="message">
    Pare o mouse sobre mim e veja a dica interligada dinamicamente!
  </span>
</div>
```
`script.js`
```javascript
var app = new Vue({
  el: '#app',
  data: {
    message: 'Você carregou esta página em ' + new Date().toLocaleString()
  }
})
```
`Entendimento:` *mantenha o atributo title do elemento sempre atualizado em relação à propriedade message da instância Vue*

## 2.3 `Condicionais e Laços`
### 2.3.1 v-if e v-else
`index.html`
```html
<div id="app">
  <p v-if="seen">Agora você me viu</p>
  <p v-else>Agora não ver mais</p>
</div>
```
`script.js`
```javascript
var app = new Vue({
  el: '#app',
  data: {
    seen: true
  }
})
```

### 2.3.2 v-for
`index.html`
```html
<div id="app">
  <ol>
    <li v-for="todo in todos">
      {{ todo.text }}
    </li>
  </ol>
</div>
```
`script.js`
```javascript
var app = new Vue({
  el: '#app',
  data: {
    todos: [
      { text: 'Aprender JavaScript' },
      { text: 'Aprender Vue' },
      { text: 'Criar algo incrível' }
    ]
  }
})
```

## 2.4 `Tratando Interação do Usuário`
`index.html`
```html
<div id="app">
  <p>{{ message }}</p>
  <button v-on:click="reverseMessage">Inverter Mensagem</button>
</div>
```
`script.js`
```javascript
var app = new Vue({
  el: '#app',
  data: {
    message: 'Olá Vue!'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})
```

## 2.4 `Two-Way Binding`
`index.html`
```html
<div id="app">
  <p>{{ message }}</p>
  <input v-model="message">
</div>
```
`script.js`
```javascript
var app = new Vue({
  el: '#app',
  data: {
    message: 'Olá Vue!'
  }
})
```

## 2.5 `Composição com Componentes`
`index.html`
```html
<div id="app">
  <ol>
    <todo-item
      v-for="item in groceryList"
      v-bind:todo="item"
      v-bind:key="item.id"
    ></todo-item>
  </ol>
</div>
```
`script.js`
```javascript
Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

var app = new Vue({
  el: '#app',
  data: {
    groceryList: [
      { id: 0, text: 'Vegetais' },
      { id: 1, text: 'Queijo' },
      { id: 2, text: 'Qualquer outra coisa que humanos podem comer' }
    ]
  }
})
```
> Conceito
```html
<div id="app">
  <app-nav></app-nav>
  <app-view>
    <app-sidebar></app-sidebar>
    <app-content></app-content>
  </app-view>
</div>
```