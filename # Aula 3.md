# Aula 3
A Instância Vue

`Uma aplicação Vue consiste em uma instância Vue raiz criada com new Vue, opcionalmente organizada em uma árvore de componentes reutilizáveis aninhados.`

## 3.1 `Dados e Métodos`
- scopo `data` e sua reatividade

### 3.1.1 Propriedades de dados de um componente
Vue expõem uma quantidade relevante de propriedades e métodos. Estes são diferenciados pelo prefixo $ para não confundi-los com propriedades definidas pelo usuário. Por exemplo:

```javascript
var data = { a: 1 }
var vm = new Vue({
  el: '#exemplo',
  data: data
})

vm.$data === data // => true
vm.$el === document.getElementById('exemplo') // => true

// $watch é um método da instância
vm.$watch('a', function (newValue, oldValue) {
  // Esta função será executada quando `vm.a` mudar
})
```

## 3.2 `Ciclo de Vida da Instância`
<p align="center">
    <img src="assets/lifecycle.png?v=4&s=200" width="500" alt="atomic designer">
</p>

```javascript
new Vue({
  data: {
    a: 1
  },
  created: function () {
    // `this` aponta para a instância
    console.log('a é: ' + this.a)
  }
})
```

`ATENÇÃO:`

Não utilize <span style="color: darkgreen">**arrow functions**</span> em propriedades de opções ou callback, como em:

<span style="color: brown">created: () => console.log(this.a)</span><br>
<span style="color: brown">vm.$watch('a', newValue => this.myMethod())</span>

Como as arrow functions não tem um this,this será tratado como qualquer outra variável e lexicamente pesquisada através de escopos parentais até ser encontrada, frequentemente resultando em erros como :

<span style="color: brown">Uncaught TypeError: Cannot read property of undefined</span><br>
<span style="color: brown">Uncaught TypeError: this.myMethod is not a function.</span>