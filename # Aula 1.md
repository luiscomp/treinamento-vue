# Aula 1
Entendendo uma aplicação componentizada

## 1.1 `Atomic Designer`
<p align="center">
    <img src="assets/atomic-design.jpg?v=4&s=200" width="500" alt="atomic designer">
</p>

- Átomos (inputs)
- Moléculas (labels)
- Organismos (Compoonentes)
- Templates (Página pré definida)
- Páginas (Página de contúdo)

### 1.1.1 Vantagens
- Você pode misturar e combinar componentes
- Separação clara entre design e conteúdo
- Criar um guia de estilo é simples
- Layout fácil de entender
- O código é mais consistente

---

## 1.2 `O que é VueJS?`

- Framework Progressivo (Possível de "plugar" em projetos existentes)
- Não Monolítico (View Layer)

### 1.2.1 Arvore de Componentes
É comum que um aplicativo seja organizado em uma árvore de componentes aninhados
<p align="center">
    <img src="assets/components.png?v=4&s=200" width="500" alt="component tree">
</p>

---